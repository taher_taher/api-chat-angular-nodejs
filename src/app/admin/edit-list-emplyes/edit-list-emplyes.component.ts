import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ApiService } from "./../../service/api.service";

@Component({
  selector: "app-edit-list-emplyes",
  templateUrl: "./edit-list-emplyes.component.html",
  styleUrls: ["./edit-list-emplyes.component.scss"]
})
export class EditListEmplyesComponent implements OnInit {
  submitted = false;
  editForm: FormGroup;
  id;
  constructor(
    private apiService: ApiService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.id = this.actRoute.snapshot.paramMap.get("id");
    this.getEmploye(this.id);
    this.editForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      lastname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
      phone: new FormControl("", [Validators.required]),
      gender: new FormControl("", [Validators.required]),
      adress: new FormControl("", [Validators.required]),
      role: new FormControl("", [Validators.required])
    });
  }
  getEmploye(id) {
    this.apiService.getEmploye(id).subscribe((editForm: any) => {
      console.log(editForm);
      this.editForm = new FormGroup({
        name: new FormControl(editForm.name),
        lastname: new FormControl(editForm.lastname),
        email: new FormControl(editForm.email),
        password: new FormControl(editForm.password),
        phone: new FormControl(editForm.phone),
        gender: new FormControl(editForm.gender),
        adress: new FormControl(editForm.adress),
        role: new FormControl(editForm.role)
      });
    });
  }
  get myForm() {
    return this.editForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (!this.editForm.valid) {
      return false;
    } else {
      this.apiService.UpdateEmploye(this.id, this.editForm.value).subscribe(
        res => {
          this.router.navigateByUrl("/listEmploye");
          console.log("Content updated successfully!");
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
