import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditListEmplyesComponent } from './edit-list-emplyes.component';

describe('EditListEmplyesComponent', () => {
  let component: EditListEmplyesComponent;
  let fixture: ComponentFixture<EditListEmplyesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditListEmplyesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditListEmplyesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
