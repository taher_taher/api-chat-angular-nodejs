import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutEmployesComponent } from './ajout-employes.component';

describe('AjoutEmployesComponent', () => {
  let component: AjoutEmployesComponent;
  let fixture: ComponentFixture<AjoutEmployesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutEmployesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutEmployesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
