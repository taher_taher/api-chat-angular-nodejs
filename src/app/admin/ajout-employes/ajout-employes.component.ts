import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ApiService } from "./../../service/api.service";

@Component({
  selector: "app-ajout-employes",
  templateUrl: "./ajout-employes.component.html",
  styleUrls: ["./ajout-employes.component.scss"]
})
export class AjoutEmployesComponent implements OnInit {
  submitted = false;
  registerForm: FormGroup;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.registerForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      lastname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
      phone: new FormControl("", [Validators.required]),
      gender: new FormControl("", [Validators.required]),
      adress: new FormControl("", [Validators.required]),
      role: new FormControl("", [Validators.required])
    });
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm);
    if (!this.registerForm.valid) {
    } else {
      this.apiService.createUser(this.registerForm.value).subscribe(data => {
        localStorage.setItem("token", data.token);
        this.router.navigateByUrl("/home");
      });
    }
  }
}
