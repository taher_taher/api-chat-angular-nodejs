import { Component, OnInit } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";

import { ApiService } from "../../service/api.service";

@Component({
  selector: "app-list-employe",
  templateUrl: "./list-employe.component.html",
  styleUrls: ["./list-employe.component.scss"]
})
export class ListEmployeComponent implements OnInit {
  Employes: any = [];

  constructor(private apiService: ApiService) {
    this.readEmployes();
  }
  ngOnInit() {}

  readEmployes() {
    this.apiService.getEmployes().subscribe(data => {
      this.Employes = data;
    });
  }

  removeEmployes(Employes, index) {
    if (window.confirm("Are you sure?")) {
      this.apiService.DeleteEmploye(Employes._id).subscribe(data => {
        this.Employes.splice(index, 1);
      });
    }
  }
}
