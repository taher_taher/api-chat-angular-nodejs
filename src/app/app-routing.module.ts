import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { from } from "rxjs";

import { ReclamationComponent } from "./reclamation/reclamation.component";
import { AdminComponent } from "./admin/admin/admin.component";
import { AjoutEmployesComponent } from "./admin/ajout-employes/ajout-employes.component";
import { EditListEmplyesComponent } from "./admin/edit-list-emplyes/edit-list-emplyes.component";
import { ListEmployeComponent } from "./admin/list-employe/list-employe.component";
import { AboutComponent } from "./components/about/about.component";
import { ChatComponent } from "./components/chat/chat.component";
import { ChefProjetBackComponent } from "./components/chef-projet-back/chef-projet-back.component";
import { ChefProjetFrontComponent } from "./components/chef-projet-front/chef-projet-front.component";
import { ConseilComponent } from "./components/conseil/conseil.component";
import { EditRecBackComponent } from "./components/edit-rec-back/edit-rec-back.component";
import { EditRecChefBackComponent } from "./components/edit-rec-chef-back/edit-rec-chef-back.component";
import { EditRecChefFrontComponent } from "./components/edit-rec-chef-front/edit-rec-chef-front.component";
import { EditRecFrontComponent } from "./components/edit-rec-front/edit-rec-front.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { TeckBackComponent } from "./components/teck-back/teck-back.component";
import { TeckFrontComponent } from "./components/teck-front/teck-front.component";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "about" },
  { path: "about", component: AboutComponent },
  { path: "home", component: HomeComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "admin", component: AdminComponent },
  { path: "techFront", component: TeckFrontComponent },
  { path: "techBack", component: TeckBackComponent },
  { path: "chefProjetFront", component: ChefProjetFrontComponent },
  { path: "chefProjetBack", component: ChefProjetBackComponent },
  { path: "ajoutEmployes", component: AjoutEmployesComponent },
  { path: "listEmploye", component: ListEmployeComponent },
  { path: "listReclamation", component: ReclamationComponent },
  { path: "Edit-Rec-Front/:id", component: EditRecFrontComponent },
  { path: "Edit-Rec-Back/:id", component: EditRecBackComponent },
  { path: "Edit-Rec-Chef-Back/:id", component: EditRecChefBackComponent },
  { path: "Edit-Rec-Chef-Front/:id", component: EditRecChefFrontComponent },
  { path: "Edit-List-Emplyes/:id", component: EditListEmplyesComponent },
  { path: "Conseiller", component: ConseilComponent },
  { path: "chat", component: ChatComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
