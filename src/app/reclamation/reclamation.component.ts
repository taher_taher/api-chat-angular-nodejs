import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ReclamationService } from "./../service/reclamation.service";

@Component({
  selector: "app-reclamation",
  templateUrl: "./reclamation.component.html",
  styleUrls: ["./reclamation.component.scss"]
})
export class ReclamationComponent implements OnInit {
  Reclamation: any = [];

  constructor(private reclamationService: ReclamationService) {
    this.readReclamation();
  }

  ngOnInit() {}
  readReclamation() {
    this.reclamationService.getReclamations().subscribe(data => {
      this.Reclamation = data;
    });
  }
  /*removeReclamation(reclamation, index) {
if (window.confirm("Are you sure?")) {
this.reclamationService
  .deleteReclamation(reclamation._id)
  .subscribe(data => {
    this.Reclamation.splice(index, 1);
  });
}
}*/
}
