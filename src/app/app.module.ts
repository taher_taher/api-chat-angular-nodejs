import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { from } from "rxjs";

import { SocketIoModule, SocketIoConfig } from "ngx-socket-io";

import { MDBBootstrapModule } from "angular-bootstrap-md";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { ReclamationComponent } from "./reclamation/reclamation.component";
import { AdminComponent } from "./admin/admin/admin.component";
import { AjoutEmployesComponent } from "./admin/ajout-employes/ajout-employes.component";
import { EditListEmplyesComponent } from "./admin/edit-list-emplyes/edit-list-emplyes.component";
import { ListEmployeComponent } from "./admin/list-employe/list-employe.component";
import { AboutComponent } from "./components/about/about.component";
import { ChatComponent } from "./components/chat/chat.component";
import { ChefProjetBackComponent } from "./components/chef-projet-back/chef-projet-back.component";
import { ChefProjetFrontComponent } from "./components/chef-projet-front/chef-projet-front.component";
import { ConseilComponent } from "./components/conseil/conseil.component";
import { EditRecBackComponent } from "./components/edit-rec-back/edit-rec-back.component";
import { EditRecChefBackComponent } from "./components/edit-rec-chef-back/edit-rec-chef-back.component";
import { EditRecChefFrontComponent } from "./components/edit-rec-chef-front/edit-rec-chef-front.component";
import { EditRecFrontComponent } from "./components/edit-rec-front/edit-rec-front.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { RegisterComponent } from "./components/register/register.component";
import { TeckBackComponent } from "./components/teck-back/teck-back.component";
import { TeckFrontComponent } from "./components/teck-front/teck-front.component";
import { FooterComponent } from './components/footer/footer.component';

const config: SocketIoConfig = { url: "http://localhost:3006", options: {} };

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ChefProjetFrontComponent,
    ChefProjetBackComponent,
    TeckFrontComponent,
    TeckBackComponent,
    AjoutEmployesComponent,
    ReclamationComponent,
    ListEmployeComponent,
    AdminComponent,
    EditRecFrontComponent,
    EditRecBackComponent,
    ChatComponent,
    NavbarComponent,
    AboutComponent,
    EditRecChefBackComponent,
    EditRecChefFrontComponent,
    ConseilComponent,
    EditListEmplyesComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    SocketIoModule.forRoot(config)
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
