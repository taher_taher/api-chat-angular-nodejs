import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";

import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class ReclamationService {
  baseUri: string = "http://localhost:3006/reclamations";
  constructor(private http: HttpClient) {}

  //Reclamation
  /*deleteReclamation(id): Observable<any> {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}/removeReclamation/${id}`;
    return this.http.post(url, { headers: header });
  }*/
  // Create Reclamation
  createReclamation(data): Observable<any> {
    let url = `${this.baseUri}/CreatReclamation`;
    return this.http.post(url, data);
  }
  // Get all Reclamation
  /*getReclamations() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}/getAllReclamationsAdmin`, {
      headers: header
    });
  }*/
  getReclamationsBack() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}/getAllReclamationsBack`, {
      headers: header
    });
  }
  getReclamationsFront() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}/getAllReclamationsFront`, {
      headers: header
    });
  }
  getOneReclamationsFront(id) {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}/getOneReclamationFront/${id}`;
    return this.http.get(url, { headers: header }).pipe(
      map((res: Response) => {
        return res || {};
      }),
      catchError(this.errorMgmt)
    );
  }
  getOneReclamationsBack(id) {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}/getOneReclamationBack/${id}`;
    return this.http.get(url, { headers: header }).pipe(
      map((res: Response) => {
        return res || {};
      }),
      catchError(this.errorMgmt)
    );
  }

  //update Reclamation
  EditReclamation(id, data): Observable<any> {
    let header = new HttpHeaders();
    header = header.append(
      "authorization",
      "bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}/UpdateReclamation/${id}`;
    return this.http.post(url, data, { headers: header });
  }
  // Get all employees
  getReclamations() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}/getAllReclamationsAdmin`, {
      headers: header
    });
  }
  // Error handling
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
