import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";

import { Observable, throwError } from "rxjs";

import * as jwt_decode from "jwt-decode";

import { catchError, map } from "rxjs/operators";

import * as io from "socket.io-client";
@Injectable({
  providedIn: "root"
})
export class ApiService {
  baseUri: string = "http://localhost:3006/users/";
  connectedUser: any;
  socket: any;
  constructor(private http: HttpClient, private router: Router) {
    this.connectedUser = this.getConnectedUser();
  }

  //login
  loginUser(data): Observable<any> {
    let url = `${this.baseUri}login`;
    return this.http.post(url, data);
  }

  // Create
  createUser(data): Observable<any> {
    let url = `${this.baseUri}register`;
    return this.http.post(url, data);
  }
  getConnectedUser() {
    if (localStorage.getItem("token")) {
      return jwt_decode(localStorage.getItem("token"))["data"];
    }
    return null;
  }
  // Get all User
  getEmployes() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}getAllUser`, { headers: header });
  }
  getConseiller() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}getAllConseiller`, {
      headers: header
    });
  }
  getClient() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}getAllClient`, { headers: header });
  }
  getEmployesFront() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}getAllUserFront`, { headers: header });
  }
  getEmployesBack() {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.get(`${this.baseUri}getAllUserBack`, { headers: header });
  }

  // Get user
  getEmploye(id): Observable<any> {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}getOneUser/${id}`;
    return this.http.get(url, { headers: header }).pipe(
      map((res: Response) => {
        return res || {};
      }),
      catchError(this.errorMgmt)
    );
  }
  // Update user
  UpdateEmploye(id, data): Observable<any> {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}updateUser/${id}`;
    return this.http.post(url, data, { headers: header });
  }
  //Update User Rec
  UpdateRecEmploye(id, idUser): Observable<any> {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}updateReclUser/${id}/${idUser}`;
    return this.http.post(url, null, { headers: header });
  }
  // Delete user
  DeleteEmploye(id): Observable<any> {
    let header = new HttpHeaders();
    header = header.append(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    let url = `${this.baseUri}/deleteUser/${id}`;
    return this.http.post(url, { headers: header });
  }
  // Error handling
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  logout() {
    localStorage.clear();
    this.connectedUser = null;
    this.router.navigateByUrl("/login");
  }
}
