import { Component, OnInit } from "@angular/core";

import { ReclamationService } from "./../../service/reclamation.service";
@Component({
  selector: "app-teck-front",
  templateUrl: "./teck-front.component.html",
  styleUrls: ["./teck-front.component.scss"]
})
export class TeckFrontComponent implements OnInit {
  Reclamation: any = [];
  constructor(private reclamationService: ReclamationService) {
    this.readReclamationFront();
  }

  ngOnInit() {}
  readReclamationFront() {
    this.reclamationService.getReclamationsFront().subscribe(data => {
      this.Reclamation = data;
    });
  }
}
