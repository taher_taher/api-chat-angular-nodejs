import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeckFrontComponent } from './teck-front.component';

describe('TeckFrontComponent', () => {
  let component: TeckFrontComponent;
  let fixture: ComponentFixture<TeckFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeckFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeckFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
