import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRecChefFrontComponent } from './edit-rec-chef-front.component';

describe('EditRecChefFrontComponent', () => {
  let component: EditRecChefFrontComponent;
  let fixture: ComponentFixture<EditRecChefFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRecChefFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRecChefFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
