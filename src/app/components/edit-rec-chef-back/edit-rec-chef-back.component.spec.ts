import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRecChefBackComponent } from './edit-rec-chef-back.component';

describe('EditRecChefBackComponent', () => {
  let component: EditRecChefBackComponent;
  let fixture: ComponentFixture<EditRecChefBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRecChefBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRecChefBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
