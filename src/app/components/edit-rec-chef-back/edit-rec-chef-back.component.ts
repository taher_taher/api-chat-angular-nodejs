import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ReclamationService } from "./../../service/reclamation.service";

@Component({
  selector: "app-edit-rec-chef-back",
  templateUrl: "./edit-rec-chef-back.component.html",
  styleUrls: ["./edit-rec-chef-back.component.scss"]
})
export class EditRecChefBackComponent implements OnInit {
  submitted = false;
  EditForm: FormGroup;
  id;

  constructor(
    private reclamationService: ReclamationService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.id = this.actRoute.snapshot.paramMap.get("id");
    this.getOneReclamationsBack(this.id);
    this.EditForm = new FormGroup({
      typeChoose: new FormControl("", [Validators.required]),
      level: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required]),
      subject: new FormControl("", [Validators.required]),
      state: new FormControl("", [Validators.required]),
      PV: new FormControl("", [Validators.required])
    });
  }
  getOneReclamationsBack(id) {
    this.reclamationService
      .getOneReclamationsBack(id)
      .subscribe((EditForm: any) => {
        console.log(EditForm);
        this.EditForm = new FormGroup({
          typeChoose: new FormControl(EditForm[0].typeChoose),
          level: new FormControl(EditForm[0].level),
          title: new FormControl(EditForm[0].title),
          subject: new FormControl(EditForm[0].subject),
          state: new FormControl(EditForm[0].state, [Validators.required]),
          PV: new FormControl(EditForm[0].PV, [Validators.required])
        });
      });
  }
  get myForm() {
    return this.EditForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (!this.EditForm.valid) {
      return false;
    } else {
      this.reclamationService
        .EditReclamation(this.id, this.EditForm.value)
        .subscribe(
          res => {
            this.router.navigateByUrl("/about");
            console.log("Content updated successfully!");
          },
          error => {
            console.log(error);
          }
        );
    }
  }
}
