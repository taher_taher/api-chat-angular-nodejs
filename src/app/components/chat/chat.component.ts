import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

import { Socket } from "ngx-socket-io";

import { ApiService } from "./../../service/api.service";
import { ChatService } from "./../../service/chat.service";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"]
})
export class ChatComponent implements OnInit {
  listeUsers: any;
  chosenUser: any;
  listeMessages: any;
  messageForm: FormGroup;
  conversation: any;
  constructor(
    private socket: Socket,
    public chatService: ChatService,
    public apiService: ApiService
  ) {
    this.listeMessages = [];
    this.listeUsers = [];
    this.messageForm = new FormGroup({
      content: new FormControl(""),
      user: new FormControl("")
    });
  }

  ngOnInit() {
    this.messageForm = new FormGroup({
      content: new FormControl(""),
      user: new FormControl(this.apiService.connectedUser._id)
    });
    this.apiService.getConseiller().subscribe((res: any) => {
      this.listeUsers = res.filter(
        obj => obj._id !== this.apiService.connectedUser._id
      );
      console.log(this.listeUsers);
      this.clickUser(this.listeUsers[0]._id);
    });
    this.socket.on("newMessageSended", () => {
      this.clickUser(this.chosenUser);
      console.log("hahahaha");
    });
  }
  clickUser(idUser) {
    this.chosenUser = idUser;
    this.chatService
      .getPrivateMessage(idUser, this.apiService.connectedUser._id)
      .subscribe((res: any) => {
        console.log(res);
        this.conversation = res._id;
        this.listeMessages = res.messages;
      });
  }
  sendMessage() {
    console.log("clicked");
    this.chatService
      .sendMessage(this.messageForm.value, this.conversation)
      .subscribe();
  }
}
