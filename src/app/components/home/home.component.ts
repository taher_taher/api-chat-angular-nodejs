import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ApiService } from "../../service/api.service";
import { ReclamationService } from "./../../service/reclamation.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  submitted = false;
  reclamationForm: FormGroup;

  constructor(
    private ReclamationService: ReclamationService,
    private apiService: ApiService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.reclamationForm = new FormGroup({
      client: new FormControl(this.apiService.connectedUser._id, [
        Validators.required
      ]),
      typeChoose: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required]),
      subject: new FormControl("", [Validators.required]),
      level: new FormControl("", [Validators.required]),
      PV: new FormControl("", [Validators.required])
    });
  }
  callType(value) {
    console.log(value);
    this.reclamationForm.controls["typeChoose"].setValue(value);
  }
  callTypeValidate(value) {
    console.log(value);
    this.reclamationForm.controls["level"].setValue(value);
  }
  onSubmit() {
    this.submitted = true;
    this.ReclamationService.createReclamation(
      this.reclamationForm.value
    ).subscribe(data => {
      this.router.navigateByUrl("/listReclamation");
    });
    console.log(this.reclamationForm);
  }
}
