import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { invalid } from "@angular/compiler/src/render3/view/util";

import { ApiService } from "./../../service/api.service";
import { ReclamationService } from "./../../service/reclamation.service";

@Component({
  selector: "app-chef-projet-front",
  templateUrl: "./chef-projet-front.component.html",
  styleUrls: ["./chef-projet-front.component.scss"]
})
export class ChefProjetFrontComponent implements OnInit {
  submitted = false;
  Reclamation: any = [];
  Employes: any = [];
  EditForm: FormGroup;
  id;
  idUser;

  constructor(
    private apiService: ApiService,
    private reclamationService: ReclamationService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.readReclamationFront();
  }

  ngOnInit() {
    this.readEmployes();
    this.id = this.actRoute.snapshot.paramMap.get("id");
    this.EditForm = new FormGroup({
      tech_Front: new FormControl("")
    });
  }
  get myForm() {
    return this.EditForm.controls;
  }
  onSubmit(id) {
    this.submitted = true;
    if (!this.EditForm.valid) {
      return false;
    } else {
      this.apiService
        .UpdateRecEmploye(id, this.EditForm.value.tech_Front)
        .subscribe(
          res => {
            this.router.navigateByUrl("/chefProjetFront");
            console.log("Content updated successfully!");
          },
          error => {
            console.log(error);
          }
        );
    }
  }
  readReclamationFront() {
    this.reclamationService.getReclamationsFront().subscribe(data => {
      this.Reclamation = data;
    });
  }
  readEmployes() {
    this.apiService.getEmployesFront().subscribe(data => {
      this.Employes = data;
    });
  }
  updateRecUser() {
    this.apiService.UpdateRecEmploye(this.id, this.idUser).subscribe(res => {
      this.router.navigateByUrl("/chefProjetFront");
      console.log("Content updated successfully!");
    });
  }
}
