import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChefProjetFrontComponent } from './chef-projet-front.component';

describe('ChefProjetFrontComponent', () => {
  let component: ChefProjetFrontComponent;
  let fixture: ComponentFixture<ChefProjetFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChefProjetFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChefProjetFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
