import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRecFrontComponent } from './edit-rec-front.component';

describe('EditRecFrontComponent', () => {
  let component: EditRecFrontComponent;
  let fixture: ComponentFixture<EditRecFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRecFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRecFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
