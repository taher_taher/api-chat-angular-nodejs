import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ReclamationService } from "./../../service/reclamation.service";

@Component({
  selector: "app-edit-rec-front",
  templateUrl: "./edit-rec-front.component.html",
  styleUrls: ["./edit-rec-front.component.scss"]
})
export class EditRecFrontComponent implements OnInit {
  submitted = false;
  editForm: FormGroup;
  id;
  constructor(
    private reclamationService: ReclamationService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.id = this.actRoute.snapshot.paramMap.get("id");
    this.getOneReclamationsFront(this.id);

    this.editForm = new FormGroup({
      typeChoose: new FormControl("", [Validators.required]),
      level: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required]),
      subject: new FormControl("", [Validators.required]),
      state: new FormControl("", [Validators.required]),
      PV: new FormControl("", [Validators.required])
    });
  }
  getOneReclamationsFront(id) {
    this.reclamationService
      .getOneReclamationsFront(id)
      .subscribe((editForm: any) => {
        console.log(editForm);
        this.editForm = new FormGroup({
          typeChoose: new FormControl(editForm[0].typeChoose),
          level: new FormControl(editForm[0].level),
          title: new FormControl(editForm[0].title),
          subject: new FormControl(editForm[0].subject),
          state: new FormControl(editForm[0].state, [Validators.required]),
          PV: new FormControl(editForm[0].PV, [Validators.required])
        });
      });
  }
  get myForm() {
    return this.editForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (!this.editForm.valid) {
      return false;
    } else {
      this.reclamationService
        .EditReclamation(this.id, this.editForm.value)
        .subscribe(
          res => {
            this.router.navigateByUrl("/about");
            console.log("Content updated successfully!");
          },
          error => {
            console.log(error);
          }
        );
    }
  }
}
