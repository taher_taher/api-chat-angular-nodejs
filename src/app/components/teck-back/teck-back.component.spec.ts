import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeckBackComponent } from './teck-back.component';

describe('TeckBackComponent', () => {
  let component: TeckBackComponent;
  let fixture: ComponentFixture<TeckBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeckBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeckBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
