import { Component, OnInit } from "@angular/core";

import { ReclamationService } from "./../../service/reclamation.service";

@Component({
  selector: "app-teck-back",
  templateUrl: "./teck-back.component.html",
  styleUrls: ["./teck-back.component.scss"]
})
export class TeckBackComponent implements OnInit {
  Reclamation: any = [];

  constructor(private reclamationService: ReclamationService) {
    this.readReclamationBack();
  }

  ngOnInit() {}
  readReclamationBack() {
    this.reclamationService.getReclamationsBack().subscribe(data => {
      this.Reclamation = data;
    });
  }
}
