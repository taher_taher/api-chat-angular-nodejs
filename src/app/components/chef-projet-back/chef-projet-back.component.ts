import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { invalid } from "@angular/compiler/src/render3/view/util";

import { ApiService } from "./../../service/api.service";
import { ReclamationService } from "./../../service/reclamation.service";

@Component({
  selector: "app-chef-projet-back",
  templateUrl: "./chef-projet-back.component.html",
  styleUrls: ["./chef-projet-back.component.scss"]
})
export class ChefProjetBackComponent implements OnInit {
  submitted = false;
  Reclamation: any = [];
  Employes: any = [];
  EditForm: FormGroup;
  id;
  idUser;

  constructor(
    private apiService: ApiService,
    private reclamationService: ReclamationService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.readReclamationBack();
  }

  ngOnInit() {
    this.readEmployes();
    this.id = this.actRoute.snapshot.paramMap.get("id");
    this.EditForm = new FormGroup({
      tech_back: new FormControl("")
    });
  }
  get myForm() {
    return this.EditForm.controls;
  }

  onSubmit(id) {
    this.submitted = true;
    if (!this.EditForm.valid) {
      return false;
    } else {
      this.apiService
        .UpdateRecEmploye(id, this.EditForm.value.tech_back)
        .subscribe(
          res => {
            this.router.navigateByUrl("/chefProjetBack");
            console.log("Content updated successfully!");
          },
          error => {
            console.log(error);
          }
        );
    }
  }
  readReclamationBack() {
    this.reclamationService.getReclamationsBack().subscribe(data => {
      this.Reclamation = data;
    });
  }
  readEmployes() {
    this.apiService.getEmployesBack().subscribe(data => {
      console.log(this.Employes);
      this.Employes = data;
    });
  }
  updateRecUser() {
    this.apiService.UpdateRecEmploye(this.id, this.idUser).subscribe(res => {
      this.router.navigateByUrl("/chefProjetBack");
      console.log("Content updated successfully!");
    });
  }
}
