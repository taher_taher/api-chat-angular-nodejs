import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChefProjetBackComponent } from './chef-projet-back.component';

describe('ChefProjetBackComponent', () => {
  let component: ChefProjetBackComponent;
  let fixture: ComponentFixture<ChefProjetBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChefProjetBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChefProjetBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
