import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRecBackComponent } from './edit-rec-back.component';

describe('EditRecBackComponent', () => {
  let component: EditRecBackComponent;
  let fixture: ComponentFixture<EditRecBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRecBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRecBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
