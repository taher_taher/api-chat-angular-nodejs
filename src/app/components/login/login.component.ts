import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { Observable, of, throwError } from "rxjs";

import { ApiService } from "../../service/api.service";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  submitted = false;
  loginForm: FormGroup;
  user: any;
  constructor(
    private apiService: ApiService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      password: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email])
    });
  }

  onSubmit() {
    this.submitted = true;
    if (!this.loginForm.valid) {
    } else {
      this.apiService.loginUser(this.loginForm.value).subscribe(data => {
        if (data.message == "wrong email") {
          return console.error("this email not exist");
        } else if (data.message == "wrong password") {
          return console.error("this password not exist");
        } else {
          localStorage.setItem("token", data.token);
          this.apiService.connectedUser = this.apiService.getConnectedUser();
          switch (this.apiService.connectedUser.role) {
            case "admin":
              this.router.navigateByUrl("/admin");
              break;
            case "ChefProjetFront":
              this.router.navigateByUrl("/chefProjetFront");
              break;
            case "ChefProjetBack":
              this.router.navigateByUrl("/chefProjetBack");
              break;
            case "TechFront":
              this.router.navigateByUrl("/techFront");
              break;
            case "TechBack":
              this.router.navigateByUrl("/techBack");
              break;
            case "Conseiller":
              this.router.navigateByUrl("/Conseiller");
              break;
            case "client":
              this.router.navigateByUrl("/home");
              break;
            default:
              this.router.navigateByUrl("/login");
          }
        }
      });
    }
    function error(message) {
      return throwError({ error: { message } });
    }
  }
}
